(function($) {
    $.wrapDetector = function(options) {
        var baselineHeight, brokeAt, charsToTestLen, id, letterSize, letterSpan, normalizedStyle, settings, testContainer, testContainerID, testDiv, theChar, i;

        settings = {
            charsToTest: " ,~`!@#$%^&*()_-+={[}]\\|;:'\"<,>.?/"
        };

        // Extend options with default settings
        if (options) {
            $.extend(settings, options);
        }

        function randomID() {
            id = "";
            for (i = 1; i <= 10; i++) {
                id += (Math.floor(Math.random() * 10) + 1);
            }
            return id;
        }

        testContainerID = randomID();

        // Create the test container
        $("<div id='WDTestArea" + testContainerID + "'></div>").appendTo("body");

        // Get a reference to it
        testContainer = $("#WDTestArea" + testContainerID);

        
        // Create the letter span.
        // This is used for finding out the width of a letter.
        // A monospace font is used to make life easier
        letterSpan = $("<span style=''>A</span>").appendTo(testContainer);

        // We want the span to grow and shrink as the letter does
        letterSpan.css({
            width: "auto",
            height: "auto",
            border: "0"
        });

        // Create the test div.
        // This will be used for testing our characters (subjects) to see which
        // of them cause the text to wrap
        testDiv = $("<div>BB</div>").appendTo(testContainer);

        normalizedStyle = {
            "font-size": "16px",
            "font-family": "Courier, \"Lucida Console\", Monospace",
            "font-weight": "normal",
            "font-style": "normal",
            "font-variant": "normal",
            "text-decoration": "none",
            "word-break": "normal",
            margin: 0,
            padding: 0,
            "visibility": "hidden" // It's important to use the visibility attribute
        // as opposed to display. This way, the elements will still take
        // up space
        };

        // Normalize some CSS attributes across the container, letter span, and test
        // div
        testContainer.add(letterSpan).add(testDiv).css(normalizedStyle);

        // Get the width of one letter
        letterSize = letterSpan.outerWidth();

        // Set the test div to twice the width of one letter.
        // The logic behind this is that three letters will either wrap, or
        // extend beyond the bounds of the div
        testDiv.css({
            width: (2 * letterSize) + "px",
            height: "auto"
        });

        // Get the base height of the test div, with only one letter in it.
        baselineHeight = testDiv.height();

        // This is the list of characters that the text broke at
        brokeAt = [];

        charsToTestLen = settings.charsToTest.length;

        // Go through each subject and test for breaks
        for (i = 0; i < charsToTestLen; i++) {
            theChar = settings.charsToTest[i];
            testDiv.text("A" + theChar + "A");
            if (testDiv.height() > baselineHeight) {
                brokeAt.push(theChar);
            }
        }

        // Cleanup
        testContainer.remove();

        return brokeAt;
    };
}(jQuery));